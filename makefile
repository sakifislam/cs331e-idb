IDB1.log:
	git log --since="Jan 9 2023" --until="Feb 16 2023" > IDB1.log

IDB2.log:
	git log --since="Feb 17 2023" --until="Mar 23 2023" > IDB2.log

IDB3.log:
	git log --since="Mar 24 2023" > IDB3.log

models.html:
	pydoc -w app/models.py

test:
	python app/tests.py