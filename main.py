from flask import Flask, render_template, request, make_response
from flask_paginate import Pagination, get_page_parameter
from app.models import app, db, Artists, Albums, Tracks

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/unittests')
def unittests():
    return render_template('unittests.html')

@app.route('/api')
def api():
    return render_template('api.html')



    
@app.route('/artist')
def artist():
    # assign artist sort criteria
    valid_columns = {
        'default': Artists.artist_popularity,
        'name': Artists.artist_name,
        'popularity': Artists.artist_popularity,
        'followers': Artists.followers,
        'track': Artists.sample_track,
        'album': Artists.sample_album,
        'genre': Artists.genres,
    }

    sort_by=None
    sort_order=None
    search_query = request.args.get('search')
    if search_query:
        # perform a search for artist names that match the query
        artist_list = db.session.query(Artists).filter(Artists.artist_name.ilike(f'%{search_query}%')).all()
    else:
        sort_by = request.args.get('sort_by', default='default')
        sort_order = request.args.get('sort_order', default='desc')

        if sort_by not in valid_columns:
            sort_by = 'default'

        if sort_order == 'asc':
            sort_column = valid_columns[sort_by].asc()
        else:
            sort_column = valid_columns[sort_by].desc()

        artist_list = db.session.query(Artists).order_by(sort_column).all()

    # Modify genres to clean display
    for artist in artist_list:
        genres = artist.genres
        genres = genres.split("'")
        for genre in genres:
            if genre == "[":
                genres.remove("[")
            if genre == ", ":
                genres.remove(", ")
            if genre == "]":
                genres.remove("]")
        artist.genres = genres
    sort_label = request.args.get('sort_label', default='Sort By')
    #setting up pagination
    page = request.args.get(get_page_parameter(), type=int, default=1)
    #9 items per page, nice balance for most screen sizes
    rows = int(request.args.get('rows', default = 10))
    per_page = rows * 3
    offset = (page - 1) * per_page
    row_label = request.args.get('row_label', default='Rows')
    # Paginated artists
    paginated_artists = artist_list[offset:offset+per_page]
    pagination = Pagination(page=page, per_page=per_page, total=len(artist_list))
    # Pagination object
    return render_template('artist.html', data=paginated_artists, pagination = pagination, sort_by = sort_by, sort_order = sort_order, rows = rows, sort_label = sort_label, row_label = row_label)

@app.route('/artist/<string:artist_id>')
def artist_instance(artist_id):
    artists = db.session.query(Artists).filter_by(artist_id=artist_id).all()
    # Modify genres to clean display
    if artists:
        genres = artists[0].genres
        genres = genres.split("'")
        for genre in genres:
            if genre == "[":
                genres.remove("[")
            if genre == ", ":
                genres.remove(", ")
            if genre == "]":
                genres.remove("]")
        artists[0].genres = genres
        return render_template('artist_instance.html', data=artists)

@app.route('/get_artists')
def artistjson():
    artists = Artists.query.all() 

    response = list()

    for artist in artists:
        response.append({ 
            "artist_id": artist.artist_id,
            "artist_image": artist.artist_image,
            "artist_name": artist.artist_name,
            "artist_popularity": artist.artist_popularity,
            "followers": artist.followers,
            "genres": artist.genres,
            "sample_album": artist.sample_album,
            "sample_album_id": artist.sample_album_id,
            "sample_track": artist.sample_track,
            "sample_track_id": artist.sample_track_id
        }) 

    return make_response({'artists': response}, 200)

@app.route('/album')
def album():
    sort_column_param = None
    sort_order_param = None
    # Define a dictionary of valid sort columns and their corresponding database columns
    valid_sort_columns = {
        'name': Albums.album_name,
        'popularity': Albums.album_popularity,
        'artists': Albums.album_artist,
        'release_date': Albums.release_date,
        'label': Albums.label,
        'tracks': Albums.total_tracks,
        'top_track': Albums.top_track,
    }

    # Get the search query parameter from the URL
    search_query = request.args.get('search')

    if search_query:
        # If a search query is provided, filter the albums by name that match the query
        album_list = db.session.query(Albums).filter(Albums.album_name.ilike(f'%{search_query}%')).all()
    else:
        # If no search query is provided, get the sort column and order parameters from the URL
        sort_column_param = request.args.get('sort_by', default='popularity')
        sort_order_param = request.args.get('sort_order', default='desc')

        # Check if the sort column parameter is valid, otherwise use the default sort column
        if sort_column_param not in valid_sort_columns:
            sort_column = valid_sort_columns['popularity']
        else:
            sort_column = valid_sort_columns[sort_column_param]

        # Check the sort order parameter and apply the appropriate sorting
        if sort_order_param == 'asc':
            album_list = db.session.query(Albums).order_by(sort_column.asc()).all()
        else:
            album_list = db.session.query(Albums).order_by(sort_column.desc()).all()

    # Set up pagination parameters from the URL
    page = request.args.get(get_page_parameter(), type=int, default=1)
    rows = int(request.args.get('rows', default=10))
    per_page = rows * 3
    offset = (page - 1) * per_page

    # Paginate the album list based on the pagination parameters
    paginated_albums = album_list[offset:offset+per_page]
    total_albums = len(album_list)

    # Set up the pagination object and render the album page template
    pagination = Pagination(page=page, per_page=per_page, total=total_albums)
    sort_label = request.args.get('sort_label', default='Sort By')
    row_label = request.args.get('row_label', default='Rows')

    return render_template('album.html', 
                           data=paginated_albums, 
                           pagination=pagination, 
                           sort_label=sort_label, 
                           row_label=row_label,
                           sort_by=sort_column_param, 
                           sort_order=sort_order_param,
                           rows=rows, 
                           search_query=search_query)

@app.route('/album/<string:album_id>')
def album_instance(album_id):
    albums = db.session.query(Albums).filter_by(album_id=album_id).all()
    if albums:
        return render_template('album_instance.html', data=albums)
    
@app.route('/get_albums')
def albumjson():
    albums = Albums.query.all()

    response = list()

    for album in albums:
        response.append({ 
            "album_artist": album.album_artist,
            "album_artist_id": album.album_artist_id,
            "album_id": album.album_id,
            "album_image": album.album_image,
            "album_name": album.album_name,
            "album_popularity": album.album_popularity,
            "label": album.label,
            "release_date": album.release_date,
            "top_track": album.top_track,
            "total_tracks": album.total_tracks,
            "top_track_id": album.top_track_id
        }) 

    return make_response({'albums': response}, 200)

@app.route('/tracks')
def tracks():

    sort_by = None
    sort_order = None
    # assign tracks sort criteria
    valid_columns = {
        'name': Tracks.track_name,
        'popularity': Tracks.track_popularity,
        'artist': Tracks.track_artist,
        'album': Albums.album_name,
        'danceability': Tracks.danceability,
        'loudness': Tracks.loudness,
        'tempo': Tracks.tempo,
        'energy': Tracks.energy,
    }

    search_query = request.args.get('search')
    if search_query:
        # perform a search for artist names that match the query
        track_list = db.session.query(Tracks).filter(Tracks.track_name.ilike(f'%{search_query}%')).all()
    else:
        sort_by = request.args.get('sort_by', default='popularity')
        sort_order = request.args.get('sort_order', default='desc')

        if sort_by not in valid_columns:
            sort_by = 'popularity'

        if sort_order == 'asc':
            sort_column = valid_columns[sort_by].asc()
        else:
            sort_column = valid_columns[sort_by].desc()

        track_list = db.session.query(
    	Tracks.danceability,
    	Tracks.energy,
    	Tracks.loudness,
    	Tracks.preview_url,
    	Tracks.tempo,
    	Tracks.track_artist,
    	Tracks.track_artist_id,
    	Tracks.track_id,
    	Tracks.track_image,
    	Tracks.track_name,
    	Tracks.track_popularity,
    	Albums.album_id,
    	Albums.album_name
    	).join(
    	Albums, 
    	Tracks.track_id == Albums.top_track_id
    	).order_by(sort_column).all()
    sort_label = request.args.get('sort_label', default='Sort By')
    #setting up pagination
    page = request.args.get(get_page_parameter(), type=int, default=1)
    #9 items per page, nice balance for most screen sizes
    rows = int(request.args.get('rows', default = 10))
    per_page = rows * 3
    offset = (page - 1) * per_page
    row_label = request.args.get('row_label', default='Rows')
    # Paginated tracks
    paginated_tracks = track_list[offset:offset+per_page]
    pagination = Pagination(page=page, per_page=per_page, total=len(track_list))
    # Pagination object
    return render_template('tracks.html', data=paginated_tracks, pagination = pagination, sort_by = sort_by, sort_order = sort_order, rows = rows, sort_label = sort_label, row_label = row_label)

@app.route('/track/<string:track_id>')
def tracks_instance(track_id):
    tracks = db.session.query(
        Tracks.danceability,
        Tracks.energy,
        Tracks.loudness,
        Tracks.preview_url,
        Tracks.tempo,
        Tracks.track_artist,
        Tracks.track_artist_id,
        Tracks.track_id,
        Tracks.track_image,
        Tracks.track_name,
        Tracks.track_popularity,
        Albums.album_id,
        Albums.album_name
    ).join(
        Albums,
        Tracks.track_id == Albums.top_track_id
    ).filter(
        Tracks.track_id == track_id
    ).all()
    if tracks:
        return render_template('tracks_instance.html', data=tracks)

@app.route('/get_tracks')
def trackjson():
    tracks = Tracks.query.all()

    response = list()

    for track in tracks:
        response.append({ 
            "danceability": track.danceability,
            "energy": track.energy,
            "loudness": track.loudness,
            "preview_url": track.preview_url,
            "tempo": track.tempo,
            "track_artist": track.track_artist,
            "track_id": track.track_id,
            "track_image": track.track_image,
            "track_name": track.track_name,
            "track_popularity": track.track_popularity
        }) 

    return make_response({'tracks': response}, 200)

if __name__ == '__main__':
    app.debug = True
    app.run(port = 5001)
