# -------
# imports
# -------

import unittest
from models import db, Artists, Albums, Tracks

# -----------
# DBTestCases
# -----------
class DBTestCases(unittest.TestCase):
    # ---------
    # insertion
    # ---------
    
    def test_insert_artist(self):
        a = Artists(artist_id='4MCBfE4596Uoi2O4DtmEMz', artist_name='Juice WRLD', artist_popularity=91, followers=28268587, genres=["chicago rap", "melodic rap"], 
                    artist_image='https://i.scdn.co/image/ab6761610000e5eb1908e1a8b79abf71d5598944', sample_track='All Girls Are The Same', sample_track_id='4VXIryQMWpIdGgYR4TrjT1',
                    sample_album='Goodbye & Good Riddance', sample_album_id='1typPCwqyXMfFpvDZAyKew')
        db.session.add(a)
        db.session.commit()

        b = db.session.query(Artists).filter_by(artist_id = '4MCBfE4596Uoi2O4DtmEMz').one()
        self.assertEqual(str(b.artist_id), '4MCBfE4596Uoi2O4DtmEMz')
        self.assertEqual(str(b.artist_name), 'Juice WRLD')
        self.assertEqual(str(b.artist_popularity), '91')
        self.assertEqual(str(b.followers), '28268587')
        self.assertEqual(str(b.sample_track), 'All Girls Are The Same')
        self.assertEqual(str(b.sample_track_id), '4VXIryQMWpIdGgYR4TrjT1')
        self.assertEqual(str(b.sample_album), 'Goodbye & Good Riddance')
        self.assertEqual(str(b.sample_album_id), '1typPCwqyXMfFpvDZAyKew')

        db.session.query(Artists).filter_by(artist_id = '4MCBfE4596Uoi2O4DtmEMz').delete()
        db.session.commit()

    def test_insert_album(self):
        c = Albums(album_id='1typPCwqyXMfFpvDZAyKew', album_name='Fighting Demons (Deluxe)', album_artist='Juice WRLD', album_artist_id='4MCBfE4596Uoi2O4DtmEMz',
                   release_date='2022-03-18', total_tracks=23, album_image='https://i.scdn.co/image/ab67616d0000b2737459992b444de38842b9bee7', 
                   album_popularity=82, label='Grade A Productions/Interscope Records', top_track='Burn', top_track_id='1X8E4vVoOM3BpSQlEDSjjM')
        db.session.add(c)
        db.session.commit()

        d = db.session.query(Albums).filter_by(album_id = '1typPCwqyXMfFpvDZAyKew').one()
        self.assertEqual(str(d.album_id), '1typPCwqyXMfFpvDZAyKew')
        self.assertEqual(str(d.album_name), 'Fighting Demons (Deluxe)')
        self.assertEqual(str(d.album_artist), 'Juice WRLD')
        self.assertEqual(str(d.album_artist_id), '4MCBfE4596Uoi2O4DtmEMz')
        self.assertEqual(str(d.release_date), '2022-03-18')
        self.assertEqual(str(d.total_tracks), '23')
        self.assertEqual(str(d.album_popularity), '82')
        self.assertEqual(str(d.label), 'Grade A Productions/Interscope Records')
        self.assertEqual(str(d.top_track), 'Burn')
        self.assertEqual(str(d.top_track_id), '1X8E4vVoOM3BpSQlEDSjjM')

        db.session.query(Albums).filter_by(album_id = '1typPCwqyXMfFpvDZAyKew').delete()
        db.session.commit()

    def test_insert_track(self):
        e = Tracks(track_id='21jGcNKet2qwijlDFuPiPb', track_name='Circles', track_artist='Post Malone', track_popularity=84, 
                   track_image='https://i.scdn.co/image/ab67616d0000b2739478c87599550dd73bfa7e02', 
                   preview_url='https://i.scdn.co/image/ab67616d0000b2739478c87599550dd73bfa7e02', 
                   danceability=0.695, loudness=-3.497, tempo=120.042, energy=0.762, track_artist_id='246dkjvS1zLTtiykXe5h60')
        db.session.add(e)
        db.session.commit()

        f = db.session.query(Tracks).filter_by(track_id = '21jGcNKet2qwijlDFuPiPb').one()
        self.assertEqual(str(f.track_id), '21jGcNKet2qwijlDFuPiPb')
        self.assertEqual(str(f.track_name), 'Circles')
        self.assertEqual(str(f.track_artist), 'Post Malone')
        self.assertEqual(str(f.track_artist_id), '246dkjvS1zLTtiykXe5h60')
        self.assertEqual(str(f.track_popularity), '84')
        self.assertEqual(str(f.danceability), '0.695')
        self.assertEqual(str(f.loudness), '-3.497')
        self.assertEqual(str(f.tempo), '120.042')
        self.assertEqual(str(f.energy), '0.762')

        db.session.query(Tracks).filter_by(track_id = '21jGcNKet2qwijlDFuPiPb').delete()
        db.session.commit()
        
    def test_sort_pop_artist(self):
        a1 = Artists(artist_id='4oUHIQIBe0LHzYfvXNW4QM', artist_name='Morgan Wallen', artist_popularity=94, followers=5284589, genres=['contemporary country'], 
                    artist_image='https://i.scdn.co/image/ab6761610000e5eb0266f72f16d98a972ed1ee4d', sample_track='Born With A Beer In My Hand', sample_track_id='1BAU6v0fuAV914qMUEESR1',
                    sample_album='One Thing At A Time', sample_album_id='6i7mF7whyRJuLJ4ogbH2wh')
        a2 = Artists(artist_id='7tYKF4w9nC0nq9CsPZTHyP', artist_name='SZA', artist_popularity=95, followers=11426388, genres=['pop', 'r&b'], 
                    artist_image='https://i.scdn.co/image/ab6761610000e5eb7eb7f6371aad8e67e01f0a03', sample_track='SOS', sample_track_id='5xMw6qCcpd2gBXPGTegC4W',
                    sample_album='SOS', sample_album_id='07w0rG5TETcyihsEIZR3qG')
        a3 = Artists(artist_id='0iEtIxbK0KxaSlF7G42ZOp', artist_name='Metro Boomin', artist_popularity=92, followers=3904712, genres=['rap'], 
                    artist_image='https://i.scdn.co/image/ab6761610000e5ebdf9a1555f53a20087b8c5a5c', sample_track='On Time (with John Legend)', sample_track_id='0YFqKxV9uNu6LUeYkLOKRS',
                    sample_album='HEROES & VILLAINS', sample_album_id='7txGsnDSqVMoRl6RQ9XyZP')
        
        db.session.add(a1)
        db.session.add(a2)
        db.session.add(a3)
        db.session.commit()
        
        valid_columns = {
        'default': Artists.artist_popularity,
        'name': Artists.artist_name,
        'popularity': Artists.artist_popularity,
        'followers': Artists.followers,
        'track': Artists.sample_track,
        'album': Artists.sample_album,
        'genre': Artists.genres,
        }
        sort_by='popularity'
        sort_order='desc'
        
        if sort_by not in valid_columns:
            sort_by = 'default'

        if sort_order == 'asc':
            sort_column = valid_columns[sort_by].asc()
        else:
            sort_column = valid_columns[sort_by].desc()

        artist_list = db.session.query(Artists).order_by(sort_column).all()
        
        self.assertEqual(str(artist_list[0].artist_id), '7tYKF4w9nC0nq9CsPZTHyP')
        self.assertEqual(str(artist_list[-1].artist_id), '0iEtIxbK0KxaSlF7G42ZOp')
        
        db.session.query(Artists).filter_by(artist_id = '4oUHIQIBe0LHzYfvXNW4QM').delete()
        db.session.query(Artists).filter_by(artist_id = '7tYKF4w9nC0nq9CsPZTHyP').delete()
        db.session.query(Artists).filter_by(artist_id = '0iEtIxbK0KxaSlF7G42ZOp').delete()
        db.session.commit()
        
    def test_sort_follower_artist(self):
        a1 = Artists(artist_id='4oUHIQIBe0LHzYfvXNW4QM', artist_name='Morgan Wallen', artist_popularity=94, followers=5284589, genres=['contemporary country'], 
                    artist_image='https://i.scdn.co/image/ab6761610000e5eb0266f72f16d98a972ed1ee4d', sample_track='Born With A Beer In My Hand', sample_track_id='1BAU6v0fuAV914qMUEESR1',
                    sample_album='One Thing At A Time', sample_album_id='6i7mF7whyRJuLJ4ogbH2wh')
        a2 = Artists(artist_id='7tYKF4w9nC0nq9CsPZTHyP', artist_name='SZA', artist_popularity=95, followers=11426388, genres=['pop', 'r&b'], 
                    artist_image='https://i.scdn.co/image/ab6761610000e5eb7eb7f6371aad8e67e01f0a03', sample_track='SOS', sample_track_id='5xMw6qCcpd2gBXPGTegC4W',
                    sample_album='SOS', sample_album_id='07w0rG5TETcyihsEIZR3qG')
        a3 = Artists(artist_id='0iEtIxbK0KxaSlF7G42ZOp', artist_name='Metro Boomin', artist_popularity=92, followers=3904712, genres=['rap'], 
                    artist_image='https://i.scdn.co/image/ab6761610000e5ebdf9a1555f53a20087b8c5a5c', sample_track='On Time (with John Legend)', sample_track_id='0YFqKxV9uNu6LUeYkLOKRS',
                    sample_album='HEROES & VILLAINS', sample_album_id='7txGsnDSqVMoRl6RQ9XyZP')
        
        db.session.add(a1)
        db.session.add(a2)
        db.session.add(a3)
        db.session.commit()
    
        valid_columns = {
        'default': Artists.artist_popularity,
        'name': Artists.artist_name,
        'popularity': Artists.artist_popularity,
        'followers': Artists.followers,
        'track': Artists.sample_track,
        'album': Artists.sample_album,
        'genre': Artists.genres,
        }
        sort_by='followers'
        sort_order='asc'
        
        if sort_by not in valid_columns:
            sort_by = 'default'

        if sort_order == 'asc':
            sort_column = valid_columns[sort_by].asc()
        else:
            sort_column = valid_columns[sort_by].desc()

        artist_list = db.session.query(Artists).order_by(sort_column).all()
        
        self.assertEqual(str(artist_list[0].artist_id), '0iEtIxbK0KxaSlF7G42ZOp')
        self.assertEqual(str(artist_list[-1].artist_id), '7tYKF4w9nC0nq9CsPZTHyP')
        
        db.session.query(Artists).filter_by(artist_id = '4oUHIQIBe0LHzYfvXNW4QM').delete()
        db.session.query(Artists).filter_by(artist_id = '7tYKF4w9nC0nq9CsPZTHyP').delete()
        db.session.query(Artists).filter_by(artist_id = '0iEtIxbK0KxaSlF7G42ZOp').delete()
        db.session.commit()
        
    def test_sort_genre_artist(self):
        a1 = Artists(artist_id='4oUHIQIBe0LHzYfvXNW4QM', artist_name='Morgan Wallen', artist_popularity=94, followers=5284589, genres=['contemporary country'], 
                    artist_image='https://i.scdn.co/image/ab6761610000e5eb0266f72f16d98a972ed1ee4d', sample_track='Born With A Beer In My Hand', sample_track_id='1BAU6v0fuAV914qMUEESR1',
                    sample_album='One Thing At A Time', sample_album_id='6i7mF7whyRJuLJ4ogbH2wh')
        a2 = Artists(artist_id='7tYKF4w9nC0nq9CsPZTHyP', artist_name='SZA', artist_popularity=95, followers=11426388, genres=['pop', 'r&b'], 
                    artist_image='https://i.scdn.co/image/ab6761610000e5eb7eb7f6371aad8e67e01f0a03', sample_track='SOS', sample_track_id='5xMw6qCcpd2gBXPGTegC4W',
                    sample_album='SOS', sample_album_id='07w0rG5TETcyihsEIZR3qG')
        a3 = Artists(artist_id='0iEtIxbK0KxaSlF7G42ZOp', artist_name='Metro Boomin', artist_popularity=92, followers=3904712, genres=['rap'], 
                    artist_image='https://i.scdn.co/image/ab6761610000e5ebdf9a1555f53a20087b8c5a5c', sample_track='On Time (with John Legend)', sample_track_id='0YFqKxV9uNu6LUeYkLOKRS',
                    sample_album='HEROES & VILLAINS', sample_album_id='7txGsnDSqVMoRl6RQ9XyZP')
        
        db.session.add(a1)
        db.session.add(a2)
        db.session.add(a3)
        db.session.commit()
    
        valid_columns = {
        'default': Artists.artist_popularity,
        'name': Artists.artist_name,
        'popularity': Artists.artist_popularity,
        'followers': Artists.followers,
        'track': Artists.sample_track,
        'album': Artists.sample_album,
        'genre': Artists.genres,
        }
        sort_by='genre'
        sort_order='asc'
        
        if sort_by not in valid_columns:
            sort_by = 'default'

        if sort_order == 'asc':
            sort_column = valid_columns[sort_by].asc()
        else:
            sort_column = valid_columns[sort_by].desc()

        artist_list = db.session.query(Artists).order_by(sort_column).all()
        
        self.assertEqual(str(artist_list[0].artist_id), '4oUHIQIBe0LHzYfvXNW4QM')
        self.assertEqual(str(artist_list[-1].artist_id), '0iEtIxbK0KxaSlF7G42ZOp')
        
        db.session.query(Artists).filter_by(artist_id = '4oUHIQIBe0LHzYfvXNW4QM').delete()
        db.session.query(Artists).filter_by(artist_id = '7tYKF4w9nC0nq9CsPZTHyP').delete()
        db.session.query(Artists).filter_by(artist_id = '0iEtIxbK0KxaSlF7G42ZOp').delete()
        db.session.commit()
        
    def test_sort_name_album(self):

        a = Albums(album_id='6i7mF7whyRJuLJ4ogbH2wh', album_name='One Thing At A Time', album_artist='Morgan Wallen', album_artist_id='4oUHIQIBe0LHzYfvXNW4QM',
                   release_date='2023-03-03', total_tracks=36, album_image='https://i.scdn.co/image/ab67616d0000b273705079df9a25a28b452c1fc9', 
                   album_popularity=98, label='Big Loud Records / Mercury Records / Republic Records', top_track='Born With A Beer In My Hand', top_track_id='1BAU6v0fuAV914qMUEESR1')
        
        b = Albums(album_id='07w0rG5TETcyihsEIZR3qG', album_name='SOS', album_artist='SZA', album_artist_id='7tYKF4w9nC0nq9CsPZTHyP',
                   release_date='2022-12-09', total_tracks=23, album_image='https://i.scdn.co/image/ab67616d0000b27370dbc9f47669d120ad874ec1', 
                   album_popularity=99, label='Top Dawg Entertainment/RCA Records', top_track='SOS', top_track_id='5xMw6qCcpd2gBXPGTegC4W')
        
        c = Albums(album_id='7txGsnDSqVMoRl6RQ9XyZP', album_name='HEROES & VILLAINS', album_artist='Metro Boomin', album_artist_id='0iEtIxbK0KxaSlF7G42ZOp',
                   release_date='2022-12-02', total_tracks=15, album_image='https://i.scdn.co/image/ab67616d0000b27313e54d6687e65678d60466c2', 
                   album_popularity=96, label='Republic Records', top_track='On Time (with John Legend)', top_track_id='0YFqKxV9uNu6LUeYkLOKRS')
        
        db.session.add(a)
        db.session.add(b)
        db.session.add(c)
        db.session.commit()
        
        valid_columns = {
        'name': Albums.album_name,
        'popularity': Albums.album_popularity,
        'artists': Albums.album_artist,
        'release_date': Albums.release_date,
        'label': Albums.label,
        'tracks': Albums.total_tracks,
        'top_track': Albums.top_track,
        }
        sort_by='name'
        sort_order='asc'
        
        if sort_by not in valid_columns:
            sort_by = 'popularity'

        if sort_order == 'asc':
            sort_column = valid_columns[sort_by].asc()
        else:
            sort_column = valid_columns[sort_by].desc()

        album_list = db.session.query(Albums).order_by(sort_column).all()
        
        self.assertEqual(str(album_list[0].album_id), '7txGsnDSqVMoRl6RQ9XyZP')
        self.assertEqual(str(album_list[-1].album_id), '07w0rG5TETcyihsEIZR3qG')

        db.session.query(Albums).filter_by(album_id = '6i7mF7whyRJuLJ4ogbH2wh').delete()
        db.session.query(Albums).filter_by(album_id = '07w0rG5TETcyihsEIZR3qG').delete()
        db.session.query(Albums).filter_by(album_id = '7txGsnDSqVMoRl6RQ9XyZP').delete()
        db.session.commit()
        
    def test_sort_date_album(self):

        a = Albums(album_id='6i7mF7whyRJuLJ4ogbH2wh', album_name='One Thing At A Time', album_artist='Morgan Wallen', album_artist_id='4oUHIQIBe0LHzYfvXNW4QM',
                   release_date='2023-03-03', total_tracks=36, album_image='https://i.scdn.co/image/ab67616d0000b273705079df9a25a28b452c1fc9', 
                   album_popularity=98, label='Big Loud Records / Mercury Records / Republic Records', top_track='Born With A Beer In My Hand', top_track_id='1BAU6v0fuAV914qMUEESR1')
        
        b = Albums(album_id='07w0rG5TETcyihsEIZR3qG', album_name='SOS', album_artist='SZA', album_artist_id='7tYKF4w9nC0nq9CsPZTHyP',
                   release_date='2022-12-09', total_tracks=23, album_image='https://i.scdn.co/image/ab67616d0000b27370dbc9f47669d120ad874ec1', 
                   album_popularity=99, label='Top Dawg Entertainment/RCA Records', top_track='SOS', top_track_id='5xMw6qCcpd2gBXPGTegC4W')
        
        c = Albums(album_id='7txGsnDSqVMoRl6RQ9XyZP', album_name='HEROES & VILLAINS', album_artist='Metro Boomin', album_artist_id='0iEtIxbK0KxaSlF7G42ZOp',
                   release_date='2022-12-02', total_tracks=15, album_image='https://i.scdn.co/image/ab67616d0000b27313e54d6687e65678d60466c2', 
                   album_popularity=96, label='Republic Records', top_track='On Time (with John Legend)', top_track_id='0YFqKxV9uNu6LUeYkLOKRS')
        
        db.session.add(a)
        db.session.add(b)
        db.session.add(c)
        db.session.commit()
        
        valid_columns = {
        'name': Albums.album_name,
        'popularity': Albums.album_popularity,
        'artists': Albums.album_artist,
        'release_date': Albums.release_date,
        'label': Albums.label,
        'tracks': Albums.total_tracks,
        'top_track': Albums.top_track,
        }
        sort_by='release_date'
        sort_order='desc'
        
        if sort_by not in valid_columns:
            sort_by = 'popularity'

        if sort_order == 'asc':
            sort_column = valid_columns[sort_by].asc()
        else:
            sort_column = valid_columns[sort_by].desc()

        album_list = db.session.query(Albums).order_by(sort_column).all()
        
        self.assertEqual(str(album_list[0].album_id), '6i7mF7whyRJuLJ4ogbH2wh')
        self.assertEqual(str(album_list[-1].album_id), '7txGsnDSqVMoRl6RQ9XyZP')

        db.session.query(Albums).filter_by(album_id = '6i7mF7whyRJuLJ4ogbH2wh').delete()
        db.session.query(Albums).filter_by(album_id = '07w0rG5TETcyihsEIZR3qG').delete()
        db.session.query(Albums).filter_by(album_id = '7txGsnDSqVMoRl6RQ9XyZP').delete()
        db.session.commit()
        
    def test_sort_label_album(self):

        a = Albums(album_id='6i7mF7whyRJuLJ4ogbH2wh', album_name='One Thing At A Time', album_artist='Morgan Wallen', album_artist_id='4oUHIQIBe0LHzYfvXNW4QM',
                   release_date='2023-03-03', total_tracks=36, album_image='https://i.scdn.co/image/ab67616d0000b273705079df9a25a28b452c1fc9', 
                   album_popularity=98, label='Big Loud Records / Mercury Records / Republic Records', top_track='Born With A Beer In My Hand', top_track_id='1BAU6v0fuAV914qMUEESR1')
        
        b = Albums(album_id='07w0rG5TETcyihsEIZR3qG', album_name='SOS', album_artist='SZA', album_artist_id='7tYKF4w9nC0nq9CsPZTHyP',
                   release_date='2022-12-09', total_tracks=23, album_image='https://i.scdn.co/image/ab67616d0000b27370dbc9f47669d120ad874ec1', 
                   album_popularity=99, label='Top Dawg Entertainment/RCA Records', top_track='SOS', top_track_id='5xMw6qCcpd2gBXPGTegC4W')
        
        c = Albums(album_id='7txGsnDSqVMoRl6RQ9XyZP', album_name='HEROES & VILLAINS', album_artist='Metro Boomin', album_artist_id='0iEtIxbK0KxaSlF7G42ZOp',
                   release_date='2022-12-02', total_tracks=15, album_image='https://i.scdn.co/image/ab67616d0000b27313e54d6687e65678d60466c2', 
                   album_popularity=96, label='Republic Records', top_track='On Time (with John Legend)', top_track_id='0YFqKxV9uNu6LUeYkLOKRS')
        
        db.session.add(a)
        db.session.add(b)
        db.session.add(c)
        db.session.commit()
        
        valid_columns = {
        'name': Albums.album_name,
        'popularity': Albums.album_popularity,
        'artists': Albums.album_artist,
        'release_date': Albums.release_date,
        'label': Albums.label,
        'tracks': Albums.total_tracks,
        'top_track': Albums.top_track,
        }
        sort_by='label'
        sort_order='desc'
        
        if sort_by not in valid_columns:
            sort_by = 'popularity'

        if sort_order == 'asc':
            sort_column = valid_columns[sort_by].asc()
        else:
            sort_column = valid_columns[sort_by].desc()

        album_list = db.session.query(Albums).order_by(sort_column).all()
        
        self.assertEqual(str(album_list[0].album_id), '07w0rG5TETcyihsEIZR3qG')
        self.assertEqual(str(album_list[-1].album_id), '6i7mF7whyRJuLJ4ogbH2wh')

        db.session.query(Albums).filter_by(album_id = '6i7mF7whyRJuLJ4ogbH2wh').delete()
        db.session.query(Albums).filter_by(album_id = '07w0rG5TETcyihsEIZR3qG').delete()
        db.session.query(Albums).filter_by(album_id = '7txGsnDSqVMoRl6RQ9XyZP').delete()
        db.session.commit()
        
    def test_sort_dance_track(self):
        t1 = Tracks(track_id='1BAU6v0fuAV914qMUEESR1', track_name='Born With A Beer In My Hand', track_artist='Morgan Wallen', track_popularity=78, 
                   track_image='https://i.scdn.co/image/ab67616d0000b273705079df9a25a28b452c1fc9', 
                   preview_url='No Preview Available', 
                   danceability=0.531, loudness=-5.475, tempo=148.026, energy=0.81, track_artist_id='4oUHIQIBe0LHzYfvXNW4QM')
        t2 = Tracks(track_id='5xMw6qCcpd2gBXPGTegC4W', track_name='SOS', track_artist='SZA', track_popularity=76, 
                   track_image='https://i.scdn.co/image/ab67616d0000b27370dbc9f47669d120ad874ec1', 
                   preview_url='https://p.scdn.co/mp3-preview/0d91430a4a328573f8e4630bdca40d9d9b1dc160?cid=b122c6341a664eabb0e263802b1bec6e', 
                   danceability=0.507, loudness=-7.356, tempo=119.159, energy=0.657, track_artist_id='7tYKF4w9nC0nq9CsPZTHyP')
        t3 = Tracks(track_id='0YFqKxV9uNu6LUeYkLOKRS', track_name='On Time (with John Legend)', track_artist='Metro Boomin', track_popularity=74, 
                   track_image='https://i.scdn.co/image/ab67616d0000b27313e54d6687e65678d60466c2', 
                   preview_url='No Preview Available', 
                   danceability=0.364, loudness=-6.638, tempo=79.904, energy=0.62, track_artist_id='0iEtIxbK0KxaSlF7G42ZOp')
                   
        db.session.add(t1)
        db.session.add(t2)
        db.session.add(t3)
        db.session.commit()
    
        valid_columns = {
        'name': Tracks.track_name,
        'popularity': Tracks.track_popularity,
        'artist': Tracks.track_artist,
        'album': Albums.album_name,
        'danceability': Tracks.danceability,
        'loudness': Tracks.loudness,
        'tempo': Tracks.tempo,
        'energy': Tracks.energy,
        }
        sort_by='danceability'
        sort_order='asc'
        
        if sort_by not in valid_columns:
            sort_by = 'popularity'

        if sort_order == 'asc':
            sort_column = valid_columns[sort_by].asc()
        else:
            sort_column = valid_columns[sort_by].desc()

        track_list = db.session.query(Tracks).order_by(sort_column).all()
        
        self.assertEqual(str(track_list[0].track_id), '0YFqKxV9uNu6LUeYkLOKRS')
        self.assertEqual(str(track_list[-1].track_id), '1BAU6v0fuAV914qMUEESR1')
        
        db.session.query(Tracks).filter_by(track_id = '1BAU6v0fuAV914qMUEESR1').delete()
        db.session.query(Tracks).filter_by(track_id = '5xMw6qCcpd2gBXPGTegC4W').delete()
        db.session.query(Tracks).filter_by(track_id = '0YFqKxV9uNu6LUeYkLOKRS').delete()
        db.session.commit()
        
    def test_sort_loud_track(self):
        t1 = Tracks(track_id='1BAU6v0fuAV914qMUEESR1', track_name='Born With A Beer In My Hand', track_artist='Morgan Wallen', track_popularity=78, 
                   track_image='https://i.scdn.co/image/ab67616d0000b273705079df9a25a28b452c1fc9', 
                   preview_url='No Preview Available', 
                   danceability=0.531, loudness=-5.475, tempo=148.026, energy=0.81, track_artist_id='4oUHIQIBe0LHzYfvXNW4QM')
        t2 = Tracks(track_id='5xMw6qCcpd2gBXPGTegC4W', track_name='SOS', track_artist='SZA', track_popularity=76, 
                   track_image='https://i.scdn.co/image/ab67616d0000b27370dbc9f47669d120ad874ec1', 
                   preview_url='https://p.scdn.co/mp3-preview/0d91430a4a328573f8e4630bdca40d9d9b1dc160?cid=b122c6341a664eabb0e263802b1bec6e', 
                   danceability=0.507, loudness=-7.356, tempo=119.159, energy=0.657, track_artist_id='7tYKF4w9nC0nq9CsPZTHyP')
        t3 = Tracks(track_id='0YFqKxV9uNu6LUeYkLOKRS', track_name='On Time (with John Legend)', track_artist='Metro Boomin', track_popularity=74, 
                   track_image='https://i.scdn.co/image/ab67616d0000b27313e54d6687e65678d60466c2', 
                   preview_url='No Preview Available', 
                   danceability=0.364, loudness=-6.638, tempo=79.904, energy=0.62, track_artist_id='0iEtIxbK0KxaSlF7G42ZOp')
                   
        db.session.add(t1)
        db.session.add(t2)
        db.session.add(t3)
        db.session.commit()
        
        valid_columns = {
        'name': Tracks.track_name,
        'popularity': Tracks.track_popularity,
        'artist': Tracks.track_artist,
        'album': Albums.album_name,
        'danceability': Tracks.danceability,
        'loudness': Tracks.loudness,
        'tempo': Tracks.tempo,
        'energy': Tracks.energy,
        }
        sort_by='loudness'
        sort_order='desc'
        
        if sort_by not in valid_columns:
            sort_by = 'popularity'

        if sort_order == 'asc':
            sort_column = valid_columns[sort_by].asc()
        else:
            sort_column = valid_columns[sort_by].desc()

        track_list = db.session.query(Tracks).order_by(sort_column).all()
        
        self.assertEqual(str(track_list[0].track_id), '5xMw6qCcpd2gBXPGTegC4W')
        self.assertEqual(str(track_list[-1].track_id), '1BAU6v0fuAV914qMUEESR1')
        
        db.session.query(Tracks).filter_by(track_id = '1BAU6v0fuAV914qMUEESR1').delete()
        db.session.query(Tracks).filter_by(track_id = '5xMw6qCcpd2gBXPGTegC4W').delete()
        db.session.query(Tracks).filter_by(track_id = '0YFqKxV9uNu6LUeYkLOKRS').delete()
        db.session.commit()
        
    def test_sort_tempo_track(self):
        t1 = Tracks(track_id='1BAU6v0fuAV914qMUEESR1', track_name='Born With A Beer In My Hand', track_artist='Morgan Wallen', track_popularity=78, 
                   track_image='https://i.scdn.co/image/ab67616d0000b273705079df9a25a28b452c1fc9', 
                   preview_url='No Preview Available', 
                   danceability=0.531, loudness=-5.475, tempo=148.026, energy=0.81, track_artist_id='4oUHIQIBe0LHzYfvXNW4QM')
        t2 = Tracks(track_id='5xMw6qCcpd2gBXPGTegC4W', track_name='SOS', track_artist='SZA', track_popularity=76, 
                   track_image='https://i.scdn.co/image/ab67616d0000b27370dbc9f47669d120ad874ec1', 
                   preview_url='https://p.scdn.co/mp3-preview/0d91430a4a328573f8e4630bdca40d9d9b1dc160?cid=b122c6341a664eabb0e263802b1bec6e', 
                   danceability=0.507, loudness=-7.356, tempo=120.054, energy=0.657, track_artist_id='7tYKF4w9nC0nq9CsPZTHyP')
        t3 = Tracks(track_id='0YFqKxV9uNu6LUeYkLOKRS', track_name='On Time (with John Legend)', track_artist='Metro Boomin', track_popularity=74, 
                   track_image='https://i.scdn.co/image/ab67616d0000b27313e54d6687e65678d60466c2', 
                   preview_url='No Preview Available', 
                   danceability=0.364, loudness=-6.638, tempo=101.324, energy=0.62, track_artist_id='0iEtIxbK0KxaSlF7G42ZOp')
        
        db.session.add(t1)
        db.session.add(t2)
        db.session.add(t3)
        db.session.commit()
    
        valid_columns = {
        'name': Tracks.track_name,
        'popularity': Tracks.track_popularity,
        'artist': Tracks.track_artist,
        'album': Albums.album_name,
        'danceability': Tracks.danceability,
        'loudness': Tracks.loudness,
        'tempo': Tracks.tempo,
        'energy': Tracks.energy,
        }
        sort_by='tempo'
        sort_order='asc'
        
        if sort_by not in valid_columns:
            sort_by = 'popularity'

        if sort_order == 'asc':
            sort_column = valid_columns[sort_by].asc()
        else:
            sort_column = valid_columns[sort_by].desc()

        track_list = db.session.query(Tracks).order_by(sort_column).all()
        
        self.assertEqual(str(track_list[0].track_id), '0YFqKxV9uNu6LUeYkLOKRS')
        self.assertEqual(str(track_list[-1].track_id), '1BAU6v0fuAV914qMUEESR1')
        
        db.session.query(Tracks).filter_by(track_id = '1BAU6v0fuAV914qMUEESR1').delete()
        db.session.query(Tracks).filter_by(track_id = '5xMw6qCcpd2gBXPGTegC4W').delete()
        db.session.query(Tracks).filter_by(track_id = '0YFqKxV9uNu6LUeYkLOKRS').delete()
        db.session.commit()
        
if __name__ == '__main__':
    unittest.main()
